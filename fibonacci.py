#num = input("Enter number till fibonacci is calculated: ")
#a,b=0,1
#for n in range(num):
#	print a,
#	a,b=b,a+b

#def fibo(num):
#	series=[0,1]
#	for i in range(num-2):
#		series.append(series[-1] + series[-2])
#	return series

def fibo(num):
	a,b=0,1
	for n in range(num):
		yield a
		a,b=b,a+b

num = input("Enter number of series: ")

for n in fibo(num): print "-->", n
