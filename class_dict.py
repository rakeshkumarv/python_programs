class Car:

	def __init__(self, name, color, price):
		self.__dict__['__data__']={}
		self.__dict__['__data__']['name']=name
		self.__dict__['__data__']['color']=color
		self.__dict__['__data__']['price']=price
		self.__dict__['__readonly__'] = ('name', 'color','price')
		print "Instantiated a car object"

	def __getattr__(self, attr):
		if attr not in self.__dict__['__data__']:
			raise AttributeError, "invalid attribute - ", +attr
		return self.__dict__['__data__'][attr]

	def __setattr__(self,attr,value):
		if attr in self.__dict__['__readonly__']:
			raise AttributeError, "cannot modify readonly attribute"
		else:
			self.__dict__['__data__'][attr]=value		
