def log(f):
	print "calling function.", f
	print "-" * 20
	f()
	print "function complete."

@log
def greet():
	print "Hello world"

@log
def welcome():
	print "welcome to python"


tasks=[]

def add_tasks(x):
	tasks.append(x)

@add_tasks
def add(x,y): return x+y

@add_tasks
def mul(x,y): return x*y

@add_tasks
def sub(x,y): return x-y

for t in tasks:
	print t(20,30)
