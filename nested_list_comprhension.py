temp=[]

matrix=[
  [10,20,30],
  [40,50,60],
  [70,80,90]
]

for rows in matrix:
	for values in rows:temp.append(values)


matrix2=[temp[::3], temp[1::3], temp[2::3]]

print matrix2

# easy way is

matrix3=[[x[0] for x in matrix], [x[1] for x in matrix], [x[2] for x in matrix]]

print matrix3

# nested list comphension

matrix4=[ [x[i] for x in matrix] for i in 0,1,2]

print matrix4	
