from os import walk
from sys import argv, stderr
from time import time

#Check for an arguments while running code
if len(argv) < 2:
        print >>stderr, "Usage %s path. " % argv[0]
        exit(1)

#declare an empty dictionary
directory_structure={}

#Loop through every directory which has files
for path, dirs, files in walk(argv[1]):
	if len(dirs) == 0:
                directory_structure[path]=files


print "Directory Structure is: "

#Iterate the dictionary
for key, value in directory_structure.items():
	print(key, "corresponds to", value)
