def greet(*names):
	for n in names: print "Hello", n
	print "-" * 10

greet("sam")
greet("bill", "john", "janes", "daisy")
greet("jones", "ron", "banner")
