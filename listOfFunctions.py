def add_test(x,y):
	print "Adding x and y: ", x+y
	return x+y

def mult_test(x,y):
	print "Multiplying x and y: ", x*y
	return x*y

def div_test(x,y):
	print "dividing x by y: ", x/y
	return x/y

tests = [add_test, mult_test, div_test]

values = [(10,5), (2,5), (25,2), (11,22), (12,6)]

for a,b in values:
	print "test on ", a, b
	for t in tests:
		print t(a,b)
		print "-" * 10
