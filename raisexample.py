def greet(name="Guest"):
	if type(name) is not str:
		raise TypeError, "expected string , got %s instead" % type(name)
	print "Hello", name

greet()
greet("Rakesh")
greet("rakesh", "kumar")
#greet(100)
