price = 500

class Car:
      price = 1000

      def __init__(self):
            self.price = 2000

      def get_price(self):
            print price   # Global variable
            print Car.price   # Class variable
            print self.__class__.price  # Class variable
            print self.price   # Instance variable

class SUV(Car):
      price = 3000
