class Car:

	price = 1000000  ## Class attribute

#	def __new__(Car, *args, **kwargs): ## overriding constructors, it is not recommended to override constructor
	
	def __init__(self, name, color="black"):
		print "Instantiated a Car object:", name
		self.name = name  ## Instance attributes
		self.color = color
		self.__owner = "Sam"   ## private attribute bound to only class method

	def __del__(self):
		print "Del method invoked on", self.name

	def __add__(this, other):
		c=Car(name=this.name + other.name)
		return c

	def __eq__(this, other):
		return this.price == other.price

	def drive(self, c=None): ##Instance method
		print "Driving", self.name
		print "Owner: ", self.__owner
		self.__getinfo()
		if c is not None:
			print id(c), id(self)

	def __getinfo(self):
		print "Getting information", self.name

if __name__ == '__main__':
	c1=Car(name="honda", color="grep")  ## Constructor parameters
	c2=Car("audi")

	c1.drive()
	c2.drive()

	c1.drive(c1)
