def list_contents():
	print "listing contents of directory"

def path():
	print "/home/calsoft/Desktop"

def hostname():
	print "127.0.0.1"

commands = {
	"ls"      : list_contents,
	"pwd"     : path,
	"hostname" : hostname
}

while True:
	input = raw_input("Shell> ")
	if input == "exit": break
	if input in commands:
		commands[input]()
	else:
		print "Please enter a valid command"


#	input = raw_input("Shell>")
#	if input == "ls":
#		list_contents()
#	elif input == "pwd":
#		path()
#	elif input == "hostname":
#		hostname()
#	elif input == "exit":
#		break

